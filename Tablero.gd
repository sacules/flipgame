extends TileMap

# VARIABLES
var tile_size = get_cell_size()
var half_tile_size = tile_size / 2

enum TIPO_FICHA {FICHA1, FICHA2}
enum LADO_FICHA {LADO1, LADO2}

var grid = []
var grid_size = Vector2(4,4)
var amount_of_cells = grid_size.x * grid_size.y
var dragging = false
var connected = false

onready var type1_side1_tex = load("res://sprites/Player1Side1.png")
onready var type1_side2_tex = load("res://sprites/Player1Side2.png")
onready var type2_side1_tex = load("res://sprites/Player2Side1.png")
onready var type2_side2_tex = load("res://sprites/Player2Side2.png")

# FUNCIONES
func _ready():
	# Dibuja el tablero centrado
	var window_size = OS.get_window_size()

	# El tamaño del tablero es cada celda por su tamaño individual
	position = Vector2((window_size.x - (tile_size.x * grid_size.x)) / 2, 
					   (window_size.y - (tile_size.y * grid_size.x)) / 2)

	# Inicializa el tablero
	for fila in range(grid_size.y):
		grid.append([])
		for columna in range(grid_size.x):
			grid[fila].append([null,null])

	var FichaScene = load("res://scenes/FichaSimple.tscn")

	# Fichas para testeo
	spawn_ficha(FichaScene, Vector2(0,0), [0,0])
	spawn_ficha(FichaScene, Vector2(1,3), [1,0])
	spawn_ficha(FichaScene, Vector2(0,2), [1,1])
	spawn_ficha(FichaScene, Vector2(2,3), [0,1])
	spawn_ficha(FichaScene, Vector2(1,1), [1,0])

func _draw():
	var LINE_COLOR = Color(171, 178, 191)
	var LINE_WIDTH = 2

	# Columnas
	for x in range(grid_size.x + 1):
		var col_pos = x * tile_size.x
		var limit = grid_size.y * tile_size.y
		draw_line(Vector2(col_pos, 0), Vector2(col_pos, limit), LINE_COLOR, LINE_WIDTH)

	# Filas
	for y in range(grid_size.y + 1):
		var row_pos = y * tile_size.y
		var limit = grid_size.x * tile_size.x
		draw_line(Vector2(0, row_pos), Vector2(limit, row_pos), LINE_COLOR, LINE_WIDTH)

func _process(delta):
	pass


func is_cell_vacant(pos):
	var grid_pos = world_to_map(pos)
	#prints("ficha in position", grid_pos, "is", grid[grid_pos.y][grid_pos.x])

	if (grid_pos.x >= 0 and grid_pos.x < grid_size.x and
		grid_pos.y >= 0 and grid_pos.y < grid_size.y):
		return grid[grid_pos.y][grid_pos.x] == [null, null]

	else:
		return false


func update_child_pos(ficha):

	""" 
	Actualiza el grid con la ficha de esa posición
	"""

	var grid_pos = world_to_map(ficha.original_pos)
	grid[grid_pos.y][grid_pos.x] = [null, null]

	var new_grid_pos = world_to_map(ficha.position)

	# Tipo de ficha en cierta casilla
	if ficha.TYPE == 0 and ficha.SIDE == 0:
		grid[new_grid_pos.y][new_grid_pos.x] = [FICHA1, LADO1]
	elif ficha.TYPE == 0 and ficha.SIDE == 1:
		grid[new_grid_pos.y][new_grid_pos.x] = [FICHA1, LADO2]
	elif ficha.TYPE == 1 and ficha.SIDE == 0:
		grid[new_grid_pos.y][new_grid_pos.x] = [FICHA2, LADO1]
	elif ficha.TYPE == 1 and ficha.SIDE == 1:
		grid[new_grid_pos.y][new_grid_pos.x] = [FICHA2, LADO2]

	# Testing
	#prints("\nPosición ficha", ficha.TYPE, new_grid_pos)

	print("\nTablero:")
	for x in range(len(grid)):
		prints(grid[x])
	
	
func spawn_ficha(FichaScene, pos, type_and_side):
	var Ficha = FichaScene.instance()
	add_child(Ficha)
	Ficha.scale = Vector2(0.75, 0.75)
	Ficha.TYPE = type_and_side[0]
	Ficha.SIDE = type_and_side[1]
	Ficha.original_pos = map_to_world(pos) + half_tile_size
	Ficha.position = Ficha.original_pos

	match type_and_side:
			[0,0]:
				Ficha.texture = type1_side1_tex
			[0,1]:
				Ficha.texture = type1_side2_tex
			[1,0]:
				Ficha.texture = type2_side1_tex
			[1,1]:
				Ficha.texture = type2_side2_tex

	update_child_pos(Ficha)


func check_connected(): #TODO: make more general for bigger grids!
	var fila = 0
	var columna = 0
	
	while not connected and fila < grid_size.y:
		if grid[fila][columna] != [null,null]:
			# Horizontal matches
			match columna:
				0, 1:
					if ((grid[fila][columna] == grid[fila][columna+1])
					and (grid[fila][columna] == grid[fila][columna+2])):
						connected = true

			# Vertical matches
			match fila:
				0, 1:
					if ((grid[fila][columna] == grid[fila+1][columna])
					and (grid[fila][columna] == grid[fila+2][columna])):
						connected = true

			# Diagonal matches
			match [fila,columna]:
				[0,0], [0,1], [1,0], [1,1]:
					if ((grid[fila][columna] == grid[fila+1][columna+1])
					and (grid[fila][columna] == grid[fila+2][columna+2])):
						connected = true

				[0,2], [0,3], [1,2], [1,3]:
					if ((grid[fila][columna] == grid[fila+1][columna-1]) 
					and (grid[fila][columna] == grid[fila+2][columna-2])):
						connected = true

		columna += 1

		if columna == grid_size.x:
			columna = 0
			fila += 1



# Idea de Demetrio para mover fichas sólamente a casillas al lado
# vale(for, cor, fde, cde)
#	vacía(fde,cde) and abs(abs(for-fde) + abs(cor-cde)) == 1